# Desafio Backend AWS Stage

A intenção deste projeto foi criar uma RESTApi que utiliza os recursos da Amazon AWS, para este projeto foram utilizados

- **Api Gateway**: para lidar com as rotas
- **Lambda**: Para tratar as rotas
- **DynamoDB**: Como base de dados não relacional para lidar com os dados do sistema.
- **IAM**: Para gerenciar o acesso do Node com os serviços.

O **Cognito** era a minha intenção em utilizar, mas como tive pouco tempo, literalmente 1 dia para desenvolver este aplicativo, não consegui usá-lo em sua grande parte pois encontrei problemas em implementar a autenticação. Sei que eu fiz algo de errado, mas como o tempo estava apertanto, eu preferi criar um método de autenticação eu mesmo utilizando *JWT* e banco de usuários no *Dynamo*.

## Abordagem

O sistema é um simples backend que gerencia eventos e ingressos, um administrador tem a permissão para criar shows, e usuários adquirem esses ingressos. O sistema faz verificações low level de quantidade de assentos disponíveis para permitir ou não a compra.

Usuários possuem diferentes sets de permissões que dão acessos a todo o sistema, o set completo de permissões são:

- **ListUsers** - Lista Usuários
- **CreateShow** - Cria novos Shows
- **DeleteShow** - Deleta shows
- **CreateTicket** - Cria novos tickets
- **BuyTicket** - Comprar tickets
- **CheckPermissions** - Checar as permissões do usuário

## Convenção de Commits

Para este projeto foi utilizado [Conventional Commits specification](https://www.conventionalcommits.org/en/v1.0.0/) como convenção dos commits, (baseado na convenção do [Angular](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines)), isso proporciona um set de regras para criar um histórico de confirmação explícito; o que torna mais fácil escrever ferramentas automatizadas em cima. Esta convenção se encaixa com [SemVer](https://semver.org/), descrevendo recursos, correções e alterações feitas nas mensagens de confirmação.

Para entender mais nomenclaturas do sistema de convenção, o [Karma](https://karma-runner.github.io/6.4/dev/git-commit-msg.html) possui uma maior explicação do uso.


## Instalação

Para instalar os pacotes é necessário rodar o comando:
```
npm ci
```

O sistema também necessita do [AWS CLI](https://aws.amazon.com/cli/) configurado.