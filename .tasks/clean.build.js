/*
 * Project: stage-aws
 * File: clean.build.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const fs = require("fs");
const path = require("path");
const directoryExists = require("directory-exists");

console.log("[*] Cleaning the Rendered folders...");
var _root = __dirname.replace(".tasks","");
var folders = ["build"];

//Limpa as pastas definidas no folders
folders.forEach(_x => {
    var foldername = path.resolve(_root, _x);
    if (directoryExists.sync(foldername)) {
        console.log(`[*] > Cleaning ${foldername}`);
        fs.rmSync(foldername, { recursive: true });
    }
});