/*
 * Project: stage-aws
 * File: AsyncForEach.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

/**
 * Essa função serve para substituir o Array.ForEach quando os valores dentro do for each precisam 
 * de um await unitario, não é uma boa prática porque isso gera lag no sistema, porém, mantenha 
 * essa função sendo utilizada quando o array é pequeno.
 * @param {Array<any>} array 
 * @param {Function} callback 
 */
module.exports = async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
};