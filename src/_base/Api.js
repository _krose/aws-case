/*
 * Project: stage-aws
 * File: Api.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const isArray = require("is-array");
const moment = require("moment");

/**
 * Cria uma string com parametro para leitura do banco de dados
 * @param {String} table 
 * @param {Object} data 
 * @returns Object
 */
exports.GenerateParam = (table, data = null) => {
    return Object.assign({}, { TableName: table }, data);
};

/**
 * Sanitiza um resultado para ser padronizar ao longo da api
 * @param {Boolean} status 
 * @param {Object|Array} result 
 * @returns 
 */
exports.SanitizeResult = (status, result) => {
    var _result = {
        success: status,
        requestTime: moment().format(),
        data: result
    };

    if (isArray(result)) {
        //Se for array ele vai retornar um campo adicional de contador
        return {..._result, count: result.length}
    } else {
        return result;
    }
};