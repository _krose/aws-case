/*
 * Project: stage-aws
 * File: Validation.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const isEmpty = require("is-empty");
const jwt = require("jsonwebtoken");
const { getUserById } = require("../User/controller");

const _privateKey = "9308c17e5a9e3b5e1e8b6e7dedc17c5b";

/**
 * 
 * @param {*} req 
 */
const ValidateAuth = async (req, permission) => {
    if (!isEmpty(req.headers.token)) {
        try {
            var data = jwt.verify(req.headers.token, _privateKey);
            var userData = await getUserById(data.user_id);
            if (userData.Item.permissions.includes(permission)) {
                return { authorized: true, data: data };
            } else {
                return { authorized: false, data: null };
            }
        } catch (error) {
            return { authorized: false, data: null };
        }
    } else {
        return { authorized: false, data: null }; 
    }
};

module.exports = ValidateAuth;