/*
 * Project: stage-aws
 * File: index.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const express = require("express");
const { SanitizeResult } = require("../_base/Api");
const ValidateAuth = require("../_base/Validation");
const { CreateCart, GetCart, AddToCart, DeleteItemFromCart, FinishCart } = require("./controller");

const cartRouter = express.Router();

/**
 * Cria um novo carrinho
 * @name get/cart/create
 * @requires Authentication 
 * @permissions ["BuyTicket"] 
 * @function
 */
cartRouter.get("/cart/create", async (req, res) => {
    var auth = await ValidateAuth(req, "BuyTicket");

    if (auth.authorized) {
        var cartData = await CreateCart(auth.data.user_id);
        return res.status(200).json(cartData);
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided) or your token has expired. If you're logged you can check your permissions using /user/permissions"
        }));
    }
});

/**
 * Retorna o carrinho disponível do usuário
 * @name get/cart
 * @requires Authentication 
 * @permissions ["BuyTicket"] 
 * @function
 */
cartRouter.get("/cart", async (req, res) => {
    var auth = await ValidateAuth(req, "BuyTicket");

    if (auth.authorized) {
        var getCart = await GetCart(auth.data.user_id);
        return res.status(200).json(getCart);
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided) or your token has expired. If you're logged you can check your permissions using /user/permissions"
        }));
    }
    
});

/**
 * Adiciona um novo item ao carrinho
 * @name post/cart
 * @requires Authentication 
 * @permissions ["BuyTicket"] 
 * @function
 */
cartRouter.post("/cart/add", async(req, res) => {
    var auth = await ValidateAuth(req, "BuyTicket");

    if (auth.authorized) {
        var addToCart = await AddToCart(req.body, auth.data.user_id);
        return res.status(200).json(addToCart);
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided) or your token has expired. If you're logged you can check your permissions using /user/permissions"
        }));
    }

});

/**
 * Deleta um carrinho e todos os itens dentro dele
 * @name delete/cart/item/:itemid
 * @requires Authentication 
 * @permissions ["BuyTicket"] 
 * @param {String} itemid ID do carrinho
 * @function
 */
cartRouter.delete("/cart/item/:itemid", async(req, res) => {
    var auth = await ValidateAuth(req, "BuyTicket");

    if (auth.authorized) {
        var deleteItemFromCart = await DeleteItemFromCart(req.params.itemid);
        return res.status(200).json(deleteItemFromCart);
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided) or your token has expired. If you're logged you can check your permissions using /user/permissions"
        }));
    }

});

/**
 * Adiciona um novo item ao carrinho
 * @name get/cart/finish/:idcart
 * @requires Authentication 
 * @permissions ["BuyTicket"] 
 * @param {String} idcart ID do carrinho
 * @function
 */
cartRouter.get("/cart/finish/:idcart", async(req, res) => {
    var auth = await ValidateAuth(req, "BuyTicket");

    if (auth.authorized) {
        var deleteItemFromCart = await FinishCart(req.params.idcart);
        return res.status(200).json(deleteItemFromCart);
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided) or your token has expired. If you're logged you can check your permissions using /user/permissions"
        }));
    }
});

module.exports = cartRouter;