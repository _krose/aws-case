/*
 * Project: stage-aws
 * File: controller.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const { v4: uuidv4 } = require("uuid");
const AWS = require("aws-sdk");
const moment = require("moment");
const { GenerateParam, SanitizeResult } = require("../_base/Api");
const { GetShow } = require("../Shows/controller");
const AsyncForEach = require("../_base/AsyncForEach");

const dynamoClient = new AWS.DynamoDB.DocumentClient();
const _table = "cart";

exports.CreateCart = async (user_id) => {
    //Valores padrões que são gerados para o banco de dados
    var entry = {
        cart_id: uuidv4(),
        user_id: user_id,
        createdAt: moment().format("x"),
        isClosed: false,
        isFinished: false,
        payedAt: null,
    };

    var checkCreatedCart = await this.GetCartByUserId(user_id);
    if (checkCreatedCart.length > 0) {
        if (checkCreatedCart[0].isFinished) {
            //Carrinho está finalizado, então o sistema irá criar outro
            await dynamoClient.put(GenerateParam(_table, { Item: entry })).promise();
            return SanitizeResult(true, entry);
        } else {
            //Carrinho não está finalizado, então o sistema irá retornar o carrinho atual
            return SanitizeResult(true, checkCreatedCart[0]);
        }
    } else {
        //Não existe carrinho, então um novo será criado
        await dynamoClient.put(GenerateParam(_table, { Item: entry })).promise();
        return SanitizeResult(true, entry);
    }
}

exports.AddToCart = async (data, user_id) => {
    var defaultValues = {
        show_id: null,
        qty: 0,
    };

    //Parametros finais criados
    var params = Object.assign({}, defaultValues, data);

    if (!params.show_id) {
        return SanitizeResult(false, { message: "Missing key 'show_id' in the request." });
    }

    if (!params.qty) {
        return SanitizeResult(false, { message: "Missing key 'qty' in the request." });
    }

    try {
        //Pega um carrinho já aberto recentemente, ou cria um novo
        var currentCart = await this.CreateCart(user_id);
        //Retorna os dados do show selecionado
        var selectedShow = await GetShow(params.show_id);

        //Valida para saber se o show realmente existe
        if (selectedShow.success) {
            var cartData = currentCart.data;

            if (selectedShow.data.perUser > 0) {
                //Possui um valor, então o sistema verifica a quantidade sendo comprada, coma quantidade permitida
                if (params.qty > selectedShow.data.perUser) {
                    return SanitizeResult(false, { message: `You cant buy more than ${selectedShow.data.perUser} ticket(s) per user.` }); 
                }
            }

            //Verifica se o show ainda possui lugares
            if (selectedShow.data.seats === 0) {
                return SanitizeResult(false, { message: `The event is full, not possible to buy any more tickets.` });
            }

            //Se está dentro do limite, o sistema irá criar
            var entry = {
                id: uuidv4(),
                cart_id: cartData.cart_id,
                show_id: params.show_id,
                qty: params.qty
            };

            await dynamoClient.put(GenerateParam("rel_user_cart", { Item: entry })).promise();

            return SanitizeResult(true, entry);
        } else {
            return SanitizeResult(false, { message: `Show '${params.show_id}' dont exist` });
        }

    } catch (error) {
        return SanitizeResult(false, { error });
    }
}

exports.GetItemsFromCart = async (cart_id) => {
    var params = {
        FilterExpression: "cart_id = :cart_id",
        ExpressionAttributeValues: {
            ":cart_id": cart_id
        },
        TableName: "rel_user_cart"
    }
    //return SanitizeResult(true, params);
    var list = await dynamoClient.scan(params).promise();
    return list;
};

exports.GetCart = async (user_id) => {

    try {
        //Retorna o carrinho atual do usuário
        var currentCart = await this.CreateCart(user_id);
        //Retorna os itens registrados neste carrinho
        var cartItems = await this.GetItemsFromCart(currentCart.data.cart_id);

        var itemList = [];
        var cartTotal = 0;
        await AsyncForEach(cartItems.Items, async (_item) => {
            var showInfo = await GetShow(_item.show_id);
            itemList.push({
                item_id: _item.id,
                show_id: _item.show_id,
                qty: _item.qty,
                unitaryValue: showInfo.data.value,
                totalValue: showInfo.data.value * _item.qty
            });

            cartTotal += showInfo.data.value * _item.qty;
        });

        var result = {
            ...currentCart.data,
            items: itemList,
            total: cartTotal
        }

        return SanitizeResult(true, result);
    } catch (error) {
        return SanitizeResult(false, { error });
    }
}

exports.DeleteItemFromCart = async (item_id) => {

    const params = {
        Key: { id : item_id }
    };

    try {
        await dynamoClient.delete(GenerateParam("rel_user_cart", params)).promise();
        return SanitizeResult(true, { message: "Item Deleted" });
    } catch (error) {
        return SanitizeResult(false, { error });
    }

};

exports.FinishCart = async(cart_id) => {

    try {
        const getCart = await this.GetCartById(cart_id);
        
        if (getCart) {
            //O carrinho existe, mas o sistema precisa verificar se ele já foi finalizado
            if (!getCart.isFinished) {
                
                //Aqui o sistema retorna os itens do carrinho, porque tickets vão ser criados unitariamente
                //para cada show
                const cartItems = await this.GetItemsFromCart(cart_id);
                if (cartItems.Items.length > 0) {

                    //Gera os parametros que serão criados para o update no banco de dados 
                    const params = {
                        cart_id: cart_id,
                        user_id: getCart.user_id,
                        createdAt: getCart.createdAt,
                        isClosed: true,
                        isFinished: true,
                        payedAt: moment().format()
                    };

                    //Finaliza o carrinho atualizando os dados
                    await dynamoClient.put(GenerateParam(_table, { Item: params })).promise();

                    //Processa todos os tickets comprados pela pessoa
                    await AsyncForEach(cartItems.Items, async (_item) => {
                        //Gera unitariamente os tickets dependendo da quantidade que eles foram adquiridos
                        for (let i = 0; i < _item.qty; i++) {
                            await require("../Ticket/controller").CreateTicket({ 
                                show_id: _item.show_id,
                                user_id: getCart.user_id
                            });
                        }
                    });

                    return SanitizeResult(true, params);
                } else {
                    //Carrinho não possui itens para ser finalizado
                    return SanitizeResult(false, { message: `Cart ${cart_id} is empty, cant proceed to payment gateway.` });  
                }
            } else {
                return SanitizeResult(false, { message: `Cart ${cart_id} already finished.` });    
            }
        } else {
            return SanitizeResult(false, { message: `Cart ${cart_id} dont exist.` });
        }
    } catch (error) {
        return SanitizeResult(false, { error });
    }
};

exports.GetCartById = async (cart_id) => {
    var params = {
        FilterExpression: "cart_id = :cart_id",
        ExpressionAttributeValues: {
            ":cart_id": cart_id
        },
        TableName: _table
    }

    var list = await dynamoClient.scan(params).promise();
    if (list.Items.length > 0) {
        return list.Items[0];
    } else {
        return null;
    }
};

/**
 * Retorna todos os carrinhos registrados para um determinado usuário, Esta função irá retornar 
 * os carrinhos ordenados pelo o ultimo que está aberto, esse é o unico filtro 
 * realizado por essa função.
 * @param {String} user_id 
 * @returns 
 */
exports.GetCartByUserId = async (user_id) => {
    var params = {
        FilterExpression: "user_id = :user_id",
        ExpressionAttributeValues: {
            ":user_id": user_id
        },
        TableName: _table
    }    

    //return SanitizeResult(true, params);
    var list = await dynamoClient.scan(params).promise();

    if (list.Items.length > 0) {
        return list.Items.sort((a,b) => parseInt(a.createdAt) - parseInt(b.createdAt)).reverse();
    } else {
        return [];
    }
};