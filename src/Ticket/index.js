/*
 * Project: stage-aws
 * File: index.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const express = require("express");
const { SanitizeResult } = require("../_base/Api");
const ValidateAuth = require("../_base/Validation");
const { GetTicket, CreateTicket, ClaimTicket } = require("./controller");

const ticketRouter = express.Router();

/**
 * Rota para listar todos os tickets de um usuário autenticado
 * @name get/tickets
 * @requires Authentication 
 * @permissions ["BuyTicket"] 
 * @function
 */
ticketRouter.get("/tickets", async (req, res) => {
    var auth = await ValidateAuth(req, "BuyTicket");

    if (auth.authorized) {
        var ticketData = await GetTicket(auth.data.user_id);
        return res.status(200).json(ticketData);
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided) or your token has expired. If you're logged you can check your permissions using /user/permissions"
        }));
    }
});

/**
 * Rota para retornar os dados de um determinado ticket
 * @name get/ticket/:id
 * @requires Authentication 
 * @permissions ["BuyTicket"] 
 * @param {String} id Ticket id
 * @function
 */
ticketRouter.get("/ticket/:id", async (req, res) => {
    var auth = await ValidateAuth(req, "BuyTicket");

    if (auth.authorized) {
        var ticketData = await GetTicket(auth.data.user_id, req.params.id);
        return res.status(200).json(ticketData);
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided) or your token has expired. If you're logged you can check your permissions using /user/permissions"
        }));
    }
});

/**
 * Rota para criar um ticket
 * @name post/ticket/create
 * @requires Authentication 
 * @permissions ["CreateTicket"] 
 * @function
 */
ticketRouter.post("/ticket/create", async (req, res) => {
    var auth = await ValidateAuth(req, "CreateTicket");
    
    if (auth.authorized) {
        var createTicket = await CreateTicket(req.body);
        return res.status(200).json(createTicket);
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided) or your token has expired. If you're logged you can check your permissions using /user/permissions"
        }));
    }
});

/**
 * Rota para dar claim em um ticket
 * @name post/ticket/create
 * @requires Authentication 
 * @permissions ["BuyTicket"] 
 * @param {String} tickeid ID do ticket
 * @function
 */
ticketRouter.get("/ticket/claim/:tickeid", async (req, res) => {
    var auth = await ValidateAuth(req, "BuyTicket");

    if (auth.authorized) {
        var claimTicket = await ClaimTicket(auth.data.user_id, req.params.tickeid);
        return res.status(200).json(claimTicket);
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided) or your token has expired. If you're logged you can check your permissions using /user/permissions"
        }));
    }
});

module.exports = ticketRouter;