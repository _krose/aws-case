/*
 * Project: stage-aws
 * File: controller.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const { v4: uuidv4 } = require("uuid");
const AWS = require("aws-sdk");
const moment = require("moment");
const { GenerateParam, SanitizeResult } = require("../_base/Api");

const dynamoClient = new AWS.DynamoDB.DocumentClient();
const _table = "tickets";

exports.GetTicket = async (user_id, ticket_id = null) => {
    var ticket = null;
    if (ticket_id) {
        ticket = await this.GetTicketByIdAndUser(ticket_id, user_id);
    } else {
        ticket = await this.GetTicketByUser(user_id);
    }
    
    if (ticket.Items.length > 0) {
        if (ticket_id) {
            return SanitizeResult(true, ticket.Items[0]);
        } else {
            return SanitizeResult(true, ticket.Items);
        }
    } else {
        return SanitizeResult(false, { message: `Ticket ${ticket_id} dont exist or user dont have access to it.` });
    }
};

exports.CreateTicket = async (data = {}) => {
    var defaultValue = {
        show_id: null,
        user_id: null,
    }

    var params = Object.assign({}, defaultValue, data);

    if (!params.show_id) {
        return SanitizeResult(false, { message: "Missing key 'show_id' in the request." })
    }

    var selectedShow = await  require("../Shows/controller").GetShow(params.show_id);
    if (selectedShow.success) {
        //Checa se o show ainda possui lugares
        if (selectedShow.data.availableSeats > 0) {
            var ticketData = {
                ticket_id: uuidv4(),
                show_id: params.show_id,
                user_id: params.user_id,
                createdAt: moment().format(),
                expireAt: selectedShow.data.eventDate
            };
            
            await dynamoClient.put(GenerateParam(_table, { Item: ticketData })).promise();
            return SanitizeResult(true, ticketData);
        } else {
            return SanitizeResult(false, { message: `Show '${params.show_id}' dont have any more available seats` });
        }
        
    } else {
        return SanitizeResult(false, { message: `Show '${params.show_id}' not found` });
    }
};

exports.ClaimTicket = async (user_id, ticket_id) => {
    var TicketData = await this.GetTicketById(ticket_id);
    if (TicketData.Items.length > 0) {
        if (!TicketData.Items[0].user_id) {
            var params = Object.assign({}, TicketData.Items[0], { user_id });
        
            //Faz o update dos campos
            await dynamoClient.put(GenerateParam(_table, { Item: params })).promise()
            //Retorna apenas o ticket_id e o user_id confirmando que o ticket foi reclamado com sucesso
            return SanitizeResult(true, {  ticket_id, user_id });
        } else {
            return SanitizeResult(false, { message: `Ticket '${ticket_id}' already claimed.` });    
        }
    } else {
        return SanitizeResult(false, { message: `Ticket '${ticket_id}' not found.` });
    }
};

exports.GetTicketById = async (ticket_id) => {
    var params = {
        FilterExpression: "ticket_id = :ticket_id",
        ExpressionAttributeValues: {
            ":ticket_id": ticket_id,
        },
        TableName: _table
    }

    try {
        var list = await dynamoClient.scan(params).promise();
        return list;
    } catch (error) {
        return error;
    }
};

exports.GetTicketByShow = async (show_id) => {
    var params = {
        FilterExpression: "show_id = :show_id",
        ExpressionAttributeValues: {
            ":show_id": show_id,
        },
        TableName: _table
    }

    try {
        var list = await dynamoClient.scan(params).promise();
        return list;
    } catch (error) {
        return error;
    }
}

exports.GetTicketByUser = async (user_id) => {
    var params = {
        FilterExpression: "user_id = :user_id",
        ExpressionAttributeValues: {
            ":user_id": user_id,
        },
        TableName: _table
    }

    try {
        var list = await dynamoClient.scan(params).promise();
        return list;
    } catch (error) {
        return error;
    }
};

exports.GetTicketByIdAndUser = async (ticket_id, user_id) => {
    var params = {
        FilterExpression: "ticket_id = :ticket_id AND user_id = :user_id",
        ExpressionAttributeValues: {
            ":ticket_id": ticket_id,
            ":user_id": user_id
        },
        TableName: _table
    }

    try {
        var list = await dynamoClient.scan(params).promise();
        return list;
    } catch (error) {
        return error;
    }
};