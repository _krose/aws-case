/*
 * Project: stage-aws
 * File: controller.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const { v4: uuidv4 } = require("uuid");
const AWS = require("aws-sdk");
const moment = require("moment");
const generatePassword = require("password-generator");
const passwordHash = require("password-hash");
const { SanitizeResult, GenerateParam } = require("../_base/Api");
const sha256 = require("crypto-js/sha256");
const Base64 = require("crypto-js/enc-base64");
const jwt = require("jsonwebtoken");

//Acesso do Dynamo
const dynamoClient = new AWS.DynamoDB.DocumentClient();

//const admin_permissions = ["ListUsers","CreateShow", "DeleteShow", "CreateTicket", "BuyTicket", "CheckPermissions", "GivePermissions"];
const user_permissions = ["BuyTicket", "CheckPermissions"];
const _table = "users";
const _privateKey = "9308c17e5a9e3b5e1e8b6e7dedc17c5b";

/**
 * Registra um novo usuário e atribue a ele permissões que podem ser utilizadas
 * dentro da api quando ele se logar
 * @param {Object} data Request body feito em Json com os dados
 * @returns Object
 * @example
 * //Tipo de request body aceito pela aplicçaão
 * {
 *   "name": "Name Surname",
 *   "email": "email@provider.com"
 *}
 */
exports.createNewUser = async (data = {}) => {
    //Gera um Password padrão
    var passwordGenerated = generatePassword(15, false, /[\w\d?-]/);
    //gera os dados de entrada para registrar o novo usuário
    var entry = {
        user_id: uuidv4(),
        email: data.email,
        name: data.name,
        password: passwordHash.generate(passwordGenerated),
        permissions: user_permissions,
        createdAt: moment().format(),
        isActive: true,
    };

    //Verifica se esse email já se encontra registrado
    var existingUser = await this.getUser(data.email);
    if (existingUser.Items.length === 0) {
        try {
            await dynamoClient.put(GenerateParam(_table, { Item: entry })).promise();
            return SanitizeResult(true, {
                id: entry.user_id,
                email: entry.email,
                generatedPassword: passwordGenerated
            });
        } catch (error) {
            return SanitizeResult(false, { error });
        }
    } else {
        return SanitizeResult(false, { message: `Email '${entry.email}' already registered.` });
    }
};

/**
 * Autentica um usuário e gera um token JWT referente a esse usuário, a senha será verificada nesta função
 * @param {Object} data 
 * @returns Object
 * @example
 * //Valor de entrada
 * {
 *      "email": "email@provider.com"
 *      "password": "1234"
 * }
 */
exports.AuthenticateUser = async (data = {}) => {
    var { email, password } = data;

    //Retorna o usuário pelo email
    var existingUser = await this.getUser(email);
    if (existingUser.Items.length > 0) {
        //Em ordem de autenticar o usuário, o sistema irá verificar o password
        if (passwordHash.verify(password, existingUser.Items[0].password)) {
            //Agora o sistema cria a sessão do usuário
            
            var session = {
                session_id: Base64.stringify(sha256(uuidv4())),
                user_id: existingUser.Items[0].user_id,
                startedAt: moment().format(),
            };

            //Gera um Token
            //var token = jwt.sign(session, _privateKey, { expiresIn: '2h' });
            var token = jwt.sign(session, _privateKey);
            
            return SanitizeResult(true, {
                token: token
            });
        } else {
            //Password errado
            return SanitizeResult(false, { message: "Failed to perform authentication, the email or password value is wrong." });
        }
    } else {
        return SanitizeResult(false, { message: "Failed to perform authentication, the email or password value is wrong." });
    }
};

/**
 * Retorna um usuário pelo o email dele
 * @param {String} email 
 * @returns Object
 */
exports.getUser = async (email) => {

    var params = {
        FilterExpression: "email = :email",
        ExpressionAttributeValues: {
            ":email": email
        },
        TableName: _table
    }

    try {
        const result = await dynamoClient.scan(params).promise();
        return result;
    } catch(error) {
        return null;
    }
}

/**
 * Retorna um usuário pelo o ID
 * @param {String} user_id 
 * @returns 
 */
exports.getUserById = async (user_id) => {
    const filter = { user_id };
    try {
        const result = await dynamoClient.get(GenerateParam(_table, { Key: filter })).promise();
        return result;
    } catch(error) {
        return null;
    }
};

/**
 * Retorna todos os usuários registrados na tabela
 * @returns Object
 */
exports.getAllUsers = async () => {
    try {
        const result = await dynamoClient.scan(GenerateParam(_table)).promise();
        //Cria uma lista que tenha uma visualização melhor
        var userList = [];

        result.Items.forEach(_x => {
            userList.push({
                user_id: _x.user_id,
                email: _x.email,
                name: _x.name,
                createdAt: _x.createdAt
            });
        });

        return SanitizeResult(true, userList);
    } catch(error) {
        return SanitizeResult(false, { error });
    }
};