/*
 * Project: stage-aws
 * File: index.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const express = require("express");
const { getAllUsers, createNewUser, AuthenticateUser, getUserById } = require("./controller");
const ValidateAuth = require("../_base/Validation");
const { SanitizeResult } = require("../_base/Api");

/**
 * Express router para montar as funções
 * @type {object}
 * @const
 * @namespace usersRouter
 */
const userRouter = express.Router();

/**
 * Rota para retornar todos os usuários
 * @name get/users
 * @requires Authentication
 * @permissions ["ListUsers"] 
 * @function
 */
userRouter.get("/users", async (req, res) => {
    var auth = await ValidateAuth(req, "ListUsers");

    if (auth.authorized) {
        var value = await getAllUsers();

        if (value.success) {
            return res.status(200).json(value);
        }
    
        return res.status(500).json(Object.assign({}, value, { errorMessage: "Internal error occurred" }));
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided). If you're logged you can check your permissions using /user/permissions"
        }));
    }
});

/**
 * Rota para retornar todas as permissões de um usuário
 * @name get/user/permissions
 * @requires Authentication
 * @permissions ["CheckPermissions"] 
 * @function
 */
userRouter.get("/user/permissions", async (req, res) => {

    var auth = await ValidateAuth(req, "CheckPermissions");

    if (auth.authorized) {
        var result = await getUserById(auth.data.user_id);
        return res.status(200).json(SanitizeResult(true, {
            permissions: result.Item.permissions
        }))
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided). If you're logged you can check your permissions using /user/permissions"
        }));
    }
});

/**
 * Rota para criar um novo usuário
 * @name post/users/add
 * @function
 */
userRouter.post("/users/add", async (req, res) => {
    let value = await createNewUser(req.body);

    if (value.success) {
        return res.status(200).json(value);
    }

    return res.status(500).json(Object.assign({}, value, { errorMessage: "Internal error occurred" }));
});

/**
 * Rota para autenticar um usuário
 * @name post/auth
 * @function
 */
userRouter.post("/auth", async (req, res) => {
    let value = await AuthenticateUser(req.body);

    if (value.success) {
        return res.status(200).json(value);
    }

    return res.status(500).json(Object.assign({}, value, { errorMessage: "Internal error occurred" })); 
});

module.exports = userRouter;