/*
 * Project: stage-aws
 * File: app.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const awsServerlessExpressMiddleware = require("aws-serverless-express/middleware");
const statusRouter = require("./Status");
const userRouter = require("./User");
const showsRouter = require("./Shows");
const cartRouter = require("./Cart");
const ticketRouter = require("./Ticket");

const app = express();

//Habilita o Cross-Origin
app.use(cors());
//Enforça o json no content-type
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(awsServerlessExpressMiddleware.eventContext());

app.use(function(req, res, next) {
    res.header("Content-Type", "application/json");
    next();
});

//Inicializa a API carregando as rotas
app.use("/", statusRouter, userRouter, showsRouter, cartRouter, ticketRouter);
//app.post('/test', (req, res) => {
//    res.json(req.body)
//});

module.exports = app;