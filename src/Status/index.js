/*
 * Project: stage-aws
 * File: index.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const express = require("express");

const statusRouter = express.Router();

/**
 * Retorna o status da API
 * @name get/status
 * @function
 */
statusRouter.get("/status", (req, res) => {
    res.json({
        apiStatus: "ok"
    })
});

module.exports = statusRouter;