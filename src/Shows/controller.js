/*
 * Project: stage-aws
 * File: controller.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const { v4: uuidv4 } = require("uuid");
const AWS = require("aws-sdk");
const moment = require("moment");
const { SanitizeResult, GenerateParam } = require("../_base/Api");
const isEmpty = require("is-empty");
const AsyncForEach = require("../_base/AsyncForEach");
const { GetTicketByShow } = require("../Ticket/controller");

//Acesso do Dynamo
const dynamoClient = new AWS.DynamoDB.DocumentClient();
const _table = "shows";

/**
 * Cria um novo show dentro da base de dados
 * @param {Object} data 
 */
exports.CreateNewShow = async (data = {}, authdata) => {
    //Valores padrões que serão inseridos dentro da base de dados de shows
    var defaultValues = {
        title: null,
        description: null,
        location: null,
        eventDate: null,
        seats: 0,
        value: 0,
        perUser: 0,
    };

    //Valores padrões que são gerados para o banco de dados
    var entry = {
        show_id: uuidv4(),
        user_id: authdata.user_id,
        createdAt: moment().format(),
    };

    //Parametros finais criados
    var params = Object.assign({}, entry, defaultValues, data);

    if (!params.title) {
        return SanitizeResult(false, { message: "Missing key 'title' in the request." })
    }

    if (!params.description) {
        return SanitizeResult(false, { message: "Missing key 'description' in the request." })
    }

    if (!params.location) {
        return SanitizeResult(false, { message: "Missing key 'location' in the request." })
    }

    if (!params.eventDate) {
        return SanitizeResult(false, { message: "Missing key 'eventDate' in the request." })
    }

    if (!params.seats) {
        return SanitizeResult(false, { message: "Missing key 'seats' in the request." })
    }

    if (!params.value) {
        return SanitizeResult(false, { message: "Missing key 'value' in the request." })
    }

    try {
        await dynamoClient.put(GenerateParam(_table, { Item: params })).promise();
        return SanitizeResult(true, params);
    } catch (error) {
        return SanitizeResult(false, { error });
    }
};

exports.ListAllShows = async () => {
    try {
        const result = await dynamoClient.scan(GenerateParam(_table)).promise();

        var showList = [];
        
        await AsyncForEach(result.Items, async (_item) => {
            var ticketData = await GetTicketByShow(_item.show_id);
            showList.push({ 
                ..._item,
                availableSeats: (_item.seats - ticketData.Items.length)
            });
        });
        
        return SanitizeResult(true, showList);
    } catch (error) {
        return SanitizeResult(false, { error });
    }
};

exports.GetShow = async (show_id) => {
    const params = {
        Key: { show_id }
    };

    try {
        const result = await dynamoClient.get(GenerateParam(_table, params)).promise();
        if (!isEmpty(result)) {
            var ticketData = await GetTicketByShow(show_id);
            return SanitizeResult(true, { ...result.Item, availableSeats: (result.Item.seats - ticketData.Items.length) });
        } else {
            return SanitizeResult(false, { message: `Show '${show_id}' not found` });
        }
    } catch (error) {
        return SanitizeResult(false, { error });
    }
};

exports.DeleteShow = async (show_id) => {

    const params = {
        Key: { show_id }
    };

    try {
        await dynamoClient.delete(GenerateParam(_table, params)).promise();
        return SanitizeResult(true, { message: "Show Deleted" });
    } catch (error) {
        return SanitizeResult(false, { error });
    }

};