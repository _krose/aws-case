/*
 * Project: stage-aws
 * File: index.js - Author: Miguel Couto (couttonet@gmail.com)
 * Copyright 2022 Michael Coutto
 */

const express = require("express");
const { SanitizeResult } = require("../_base/Api");
const ValidateAuth = require("../_base/Validation");
const { CreateNewShow, ListAllShows, GetShow, DeleteShow } = require("./controller");

const showsRouter = express.Router();

/**
 * Cria um novo show
 * @name post/show/create
 * @requires Authentication 
 * @permissions ["CreateShow"] 
 * @function
 */
showsRouter.post("/show/create", async (req, res) => {
    var auth = await ValidateAuth(req, "CreateShow");

    if (auth.authorized) {
        var showData = await CreateNewShow(req.body, auth.data);
        return res.status(200).json(showData);
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided) or your token has expired. If you're logged you can check your permissions using /user/permissions"
        }));
    }
});

/**
 * Lista todos os shows
 * @name get/shows
 * @function
 */
showsRouter.get("/shows", async (req,res) => {
    var allShows = await ListAllShows();
    if (allShows.success) {
        return res.status(200).json(allShows);
    }

    return res.status(500).json(Object.assign({}, { errorMessage: "Internal error occurred" }));
});

/**
 * Retorna os dados específicos de um show
 * @name get/shows
 * @param {String} id ID do show
 * @function
 */
showsRouter.get("/show/:id", async (req, res) => {
    var getShow = await GetShow(req.params.id);
    if (getShow.success) {
        return res.status(200).json(getShow);
    }

    return res.status(500).json(Object.assign({}, getShow, { errorMessage: "Internal error occurred" }));
});

/**
 * Deleta um show específico
 * @name delete/shows
 * @requires Authentication 
 * @permissions ["DeleteShow"] 
 * @param {String} id ID do show
 * @function
 */
showsRouter.delete("/show/:id", async (req, res) => {

    var auth = await ValidateAuth(req, "DeleteShow");

    if (auth.authorized) {
        var deleteShow = await DeleteShow(req.params.id);
        if (deleteShow.success) {
            return res.status(200).json(deleteShow);
        }

        return res.status(500).json(Object.assign({}, deleteShow, { errorMessage: "Internal error occurred" }));
    } else {
        return res.status(401).json(SanitizeResult(false, {
            errorMessage: "You dont have permission to access this route, or you're not logged (token not provided) or your token has expired. If you're logged you can check your permissions using /user/permissions"
        }));
    }

});

module.exports = showsRouter;